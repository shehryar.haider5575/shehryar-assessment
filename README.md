# Xe POS Short Laravel Test By Shehryar Haider
# Installation Steps

--composer Update

--npm install

--php artisan migrate

--php artisan db:seed

--php artisan storage:link
 
General Usage:

1. Laravel Auth
2. Seeder
3. Migrations
4. Storage for Assets
5. Resource Controller
6. Form Request
7. VueJs 3
8. Vite
9. Custom Components
